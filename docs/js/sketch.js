let canvasWidth = Math.min(screen.width, screen.height-150);
let canvasHeight = Math.min(screen.width, screen.height-150);
let butDrag;
let pressedButton = false;

var gravity = 0.5;
var withDrag = false;


function setup() {
  //Creating Canvas
  let canvas = createCanvas(canvasWidth, canvasHeight);
  canvas.position((screen.width-canvasWidth)/2,300);
  //canvas.center();
  //Creating Drag Button
  butDrag = createButton('Drag');
  butDrag.position(canvas.x+canvasWidth/2-butDrag.width/2,canvas.y + canvasHeight-butDrag.height, 65);
  butDrag.mousePressed(drag);
  butDrag.style('color:red')
}

var drag = function(){
  if (withDrag) { butDrag.style('color:red') }
  else { butDrag.style('color:green') }
  withDrag = !withDrag;
  pressedButton = true;
}

//Creating Pendulum  Class
var Pendulum = function(){
    this.origin = new p5.Vector(canvasWidth/2,0);
    this.Length = canvasHeight/2;
    this.radius = (3/50)*canvasHeight;
    this.angle = 0;
    this.aVelocity = 0;
    this.aAceleration = 0;
    this.position = this.getPosition();
};
Pendulum.prototype.getPosition = function() {
    var position = new p5.Vector(Math.sin(this.angle), Math.cos(this.angle));
    position.mult(this.Length);
    //println(this.direction);
    position.add(this.origin);
    return position;
};

Pendulum.prototype.draw = function() {
    //Rod
    fill(0, 0, 0);
    line(this.origin.x, this.origin.y, this.position.x, this.position.y);
    //Ball
    fill(18, 161, 39);
    ellipse(this.position.x, this.position.y, this.radius, this.radius);
}
Pendulum.prototype.update = function(){
    this.aAceleration = -1*(gravity/this.Length)*Math.sin(this.angle);
    this.aVelocity+=this.aAceleration;
    if (withDrag){this.aVelocity*=996/1000;}
    this.angle+=this.aVelocity;
    
    this.position = this.getPosition();
}

//Pendulum object
var p = new Pendulum();



mousePressed = function(){
  if (!pressedButton){
  //Unitary vector parallel to the Rod
  var direction = new p5.Vector(mouseX - p.origin.x, mouseY)
  direction.normalize();
  //New Position
  p.position = direction.copy()
  p.position.mult(p.Length).add(p.origin);
  //New Angle
  var yHat = new p5.Vector(0,1)
  p.angle = direction.angleBetween(yHat)
  //text(p.angle.toFixed(2),300,300)
  //New Angle Velocity
  p.aVelocity = 0}
  else{p.update();} //Sem esse else o pendulo pausa enquanto o botão estiver pressionado.
}
mouseReleased = function(){
  pressedButton = false
}

function draw() {
  //Background
  fill(247, 220, 113, 110);
  rect(0, 0, canvasWidth, canvasHeight);
  
  //Pendulum
  p.draw();
  if (mouseIsPressed){
    mousePressed()
    }
  else{
  p.update();
      }

  //Ground
  fill(100,153,142);
  rect(0, canvasHeight - butDrag.height, canvasWidth, butDrag.height);

}
